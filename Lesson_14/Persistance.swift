import Foundation

class Persistance {
    
    static let shared = Persistance()
    
    private let nameKey = "Persistance.nameKey"
    private let surnameKey = "Persistance.surnameKey"
    
    var name: String? {
        set { UserDefaults.standard.set(newValue, forKey: nameKey) }
        get { return UserDefaults.standard.string(forKey: nameKey) }
    }
    
    var surname: String? {
        set { UserDefaults.standard.set(newValue, forKey: surnameKey) }
        get { return UserDefaults.standard.string(forKey: surnameKey) }
    }
}
