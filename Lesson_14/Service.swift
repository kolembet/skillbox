import Foundation
import RealmSwift

protocol ServiceDelegate {
    func currentWeatherLoaded(weatherData: WeatherData)
    func forecastDataLoaded(forecastData: Listik)
}

class Service {
    
    var weatherData: WeatherData?
    var forecastData: Listik?
    var delegate: ServiceDelegate?
    private let realm = try! Realm()
    
    func loadCurrentWeather() {
        
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=Moscow,ru&APPID=1b3cafa8c16eb4aaeaee3dbbede45156&lang=ru&units=metric"
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            guard error == nil else { return }
            
            do {
                self.weatherData = try JSONDecoder().decode(WeatherData.self, from: data)
            } catch let error {
                print (error)
            }
            
            let newWeatherData = WeatherDataRealm()
            newWeatherData.temp = self.weatherData!.main.temp
            newWeatherData.weather = self.weatherData!.weather[0].description
            
            DispatchQueue.main.async {
                let oldWeatherData = self.realm.objects(WeatherDataRealm.self)
                
                try! self.realm.write {
                    self.realm.delete(oldWeatherData)
                    self.realm.add(newWeatherData)
                }
                
                self.delegate?.currentWeatherLoaded(weatherData: self.weatherData!)
            }
        }.resume()
    }
    
    func loadForecastWeather() {
        let urlString = "http://api.openweathermap.org/data/2.5/forecast?q=Moscow,ru&APPID=1b3cafa8c16eb4aaeaee3dbbede45156&lang=ru&units=metric"
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            guard error == nil else { return }
            
            do {
                self.forecastData = try JSONDecoder().decode(Listik.self, from: data)
            } catch let error {
                print (error)
            }
            
            let newForecastData = ForecastDataRealmList()
            for (index, _) in self.forecastData!.list.enumerated() {
                newForecastData.list.insert(ForecastDataRealm(), at: index)
                newForecastData.list[index].dt_txt = self.forecastData!.list[index].dt_txt
                newForecastData.list[index].temp = self.forecastData!.list[index].main.temp
                newForecastData.list[index].weather = self.forecastData!.list[index].weather[0].description
            }
            
            DispatchQueue.main.async {
                
                let oldForecastData = self.realm.objects(ForecastDataRealmList.self)
                
                try! self.realm.write {
                    self.realm.delete(oldForecastData)
                    self.realm.add(newForecastData)
                }
                
                self.delegate?.forecastDataLoaded(forecastData: self.forecastData!)
            }
        }.resume()
    }
}
