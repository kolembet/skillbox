import Foundation
import RealmSwift

class RealmPersistance: Object {
    @objc dynamic var name = ""
    @objc dynamic var done = false
}

class WeatherDataRealm: Object {
    @objc dynamic var weather = ""
    @objc dynamic var temp = 0.0
}

class ForecastDataRealm: Object {
    @objc dynamic var weather = ""
    @objc dynamic var temp = 0.0
    @objc dynamic var dt_txt = ""
}

class ForecastDataRealmList: Object {
    let list = List <ForecastDataRealm>()
}
