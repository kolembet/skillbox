import UIKit
import RealmSwift

class SecondViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let realm = try! Realm()
    
    var toDoList: Results<RealmPersistance> {
        get {
            return realm.objects(RealmPersistance.self)
        }
    }
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        
        let alert = UIAlertController(title: "Add new task", message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Item Name"
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        let addAction = UIAlertAction(title: "Save", style: .default) { (action) in
            let toDoItemTextField = (alert.textFields?.first)! as UITextField
            
            let newToDOListItem = RealmPersistance()
            newToDOListItem.name = toDoItemTextField.text!
            newToDOListItem.done = false
            
            try! self.realm.write {
                self.realm.add(newToDOListItem)
                
                self.tableView.insertRows(at: [IndexPath.init(row: self.toDoList.count - 1, section: 0)], with: .automatic)
            }
        }
        
        alert.addAction(addAction)
        present(alert, animated: true, completion: nil)
    }
}

extension SecondViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let item = toDoList[indexPath.row]
        cell.textLabel?.text = item.name
        cell.accessoryType = item.done == true ? .checkmark : .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let item = toDoList[indexPath.row]
            
            try! self.realm.write {
                self.realm.delete(item)
            }
            
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}

extension SecondViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = toDoList[indexPath.row]
        
        try! self.realm.write {
            item.done = !item.done
        }
        
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}
