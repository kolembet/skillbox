import UIKit
import CoreData

class ThirdViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var items: [NSManagedObject] = []
    var titleTextField: UITextField?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Title")
        
        do {
            items = try context.fetch(fetchRequest)
        } catch {
            print("Error during loading")
        }
        
    }
    
    func titleTextField (textField: UITextField) {
        titleTextField = textField
        titleTextField?.placeholder = "Item Name"
    }
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Add new task", message: nil, preferredStyle: .alert)
        
        let addAction = UIAlertAction(title: "Save", style: .default) { (action) in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: "Title", in: context)!
            let theTitle = NSManagedObject(entity: entity, insertInto: context)
            theTitle.setValue(self.titleTextField?.text, forKey: "title")
            
            do {
                try context.save()
                self.items.append(theTitle)
            } catch {
                print("Error during saving in Core Data")
            }
            
            self.tableView.reloadData()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(addAction)
        alert.addAction(cancelAction)
        alert.addTextField(configurationHandler: titleTextField)
        present(alert, animated: true, completion: nil)
    }
}

extension ThirdViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let title = items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = title.value(forKey: "title") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            context.delete(items[indexPath.row])
            items.remove(at: indexPath.row)
            
            do {
                try context.save()
            } catch {
                print("Error during deleting")
            }
            
            self.tableView.reloadData()
        }
    }
}
