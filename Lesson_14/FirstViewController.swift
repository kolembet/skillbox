import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var surnameLabel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateView()
    }
    
    func updateView() {
        if let name = Persistance.shared.name {
            nameLabel.text = name
        }
        
        if let surname = Persistance.shared.surname {
            surnameLabel.text = surname
        }
    }
    
    @IBAction func nameEntered(_ sender: Any) {
        Persistance.shared.name = nameLabel?.text
    }
    
    @IBAction func surnameEntered(_ sender: Any) {
        Persistance.shared.surname = surnameLabel?.text
    }
}

