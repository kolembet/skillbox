import UIKit
import RealmSwift

class FourthViewController: UIViewController {
    
    var weatherData = WeatherData()
    var forecastData = Listik()
    let service = Service()
    
    private let realm = try! Realm()
    
    var weatherDataRealm: Results<WeatherDataRealm> {
        get {
            return realm.objects(WeatherDataRealm.self)
        }
    }
    
    var forecastDataRealmList: Results<ForecastDataRealmList> {
        get {
            return realm.objects(ForecastDataRealmList.self)
        }
    }
    
    @IBOutlet weak var forecastTableView: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        service.delegate = self
        loadCurrentWeatherFromRealm()
        loadForecastWeatherFromRealm()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        service.loadCurrentWeather()
        service.loadForecastWeather()
    }
    
    func updateView(){
        tempLabel.text = "\(weatherData.main.temp)°"
        descriptionLabel.text = "\(weatherData.weather[0].description)".capitalized
        
        let currentDate = getCurrentDate()
        dateLabel.text = currentDate
    }
    
    func getCurrentDate() -> String{
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        let currentDate = "\(day).\(month).\(year)"
        return currentDate
    }
    
    func loadCurrentWeatherFromRealm() {
        guard self.realm.objects(WeatherDataRealm.self).isEmpty == true else {
            let item = weatherDataRealm.first
            tempLabel.text = "\(item!.temp)°"
            descriptionLabel.text = "\(item!.weather)".capitalized
            return
        }
    }
    
    func loadForecastWeatherFromRealm() {
        guard self.realm.objects(ForecastDataRealmList.self).isEmpty == true else {
            let item = forecastDataRealmList
            
            for (index, _) in item.first!.list.enumerated() {

                forecastData.list.insert(ForecastData(), at: index)
                forecastData.list[index].weather.insert(Weather(), at: 0)
                forecastData.list[index].dt_txt = item.first!.list[index].dt_txt
                forecastData.list[index].main.temp = item.first!.list[index].temp
                forecastData.list[index].weather[0].description = item.first!.list[index].weather
            }
            forecastTableView.reloadData()
            return
        }
    }
}


extension FourthViewController: ServiceDelegate {
    func currentWeatherLoaded(weatherData: WeatherData) {
        self.weatherData = weatherData
        updateView()
    }
    
    func forecastDataLoaded(forecastData: Listik) {
        self.forecastData = forecastData
        forecastTableView.reloadData()
    }
}

extension FourthViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecastData.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! FourthTableViewCell
        cell.dateLabel.text = forecastData.list[indexPath.row].dt_txt
        cell.descriptionLabel.text = forecastData.list[indexPath.row].weather[0].description
        cell.tempLabel.text = "\(forecastData.list[indexPath.row].main.temp)"
        return cell
    }
}
