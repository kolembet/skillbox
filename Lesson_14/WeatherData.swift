import Foundation

struct WeatherData: Codable {
    var weather: [Weather] = []
    var main = Main()
}

struct Weather: Codable {
    var id = 0
    var description = ""
}

struct Main: Codable {
    var temp = 0.0
}

struct ForecastData: Codable {
    var weather: [Weather] = []
    var main = Main()
    var dt_txt = ""
}

struct Listik: Codable {
    var list: [ForecastData] = []
}
